---
home: true
heroText: Studium
tagline: Developer Documentation
actionText: Development Guide →
actionLink: /installation
altActionText: Roadmap →
altActionLink: /roadmap
footer: Copyright @ 2021 VTK Ghent vzw. All rights reserved.
---