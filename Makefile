SHELL := /bin/bash

all: up logs
.PHONY: all

#####################
### Configuration ###
#####################
# Make docker-compose use the docker command-line interface for BuildKit support
export COMPOSE_DOCKER_CLI_BUILD := 1
# Use BuildKit to optimize builds
export DOCKER_BUILDKIT := 1
# Make sure to use the OverlayFS driver
export DOCKER_DRIVER := overlay2

# Get cache from development by default
export CI_PROJECT_PATH ?= vtkgent/site
export CI_REGISTRY_IMAGE ?= registry.gitlab.com/$(CI_PROJECT_PATH)
# Build development by default
export TARGET ?= development
export VERSION ?= $(TARGET)
export CACHE_VERSION ?= $(VERSION)

# App command shortcut
APP := docker-compose run --rm app

###################
### Credentials ###
###################
docker-login:
	@ echo "Validating docker login ..."
	@ { grep "registry.gitlab.com" "$(HOME)/.docker/config.json" && docker login registry.gitlab.com; } > /dev/null 2>&1 || { echo -e "\033[0;31merror: Docker is not logged in, please run 'docker login registry.gitlab.com'.\033[0m" && false; }
	@ echo -e "\033[0;32mDocker login valid!\033[0m"
.PHONY: docker-login

###############
### Targets ###
###############

build: docker-login
	@ # Fetch the latest image and do not fail if not found
	@ docker pull registry.gitlab.com/$(CI_PROJECT_PATH):$(CACHE_VERSION) || true
	@ # Build the new image with cache from the most recent image
	@ docker-compose build app
.PHONY: build

publish: build
	@ docker-compose push app
.PHONY: publish

up: build network
	@ docker-compose up -d
.PHONY: up

down:
	@ docker-compose down $(ARGS)
.PHONY: down

logs:
	@ docker-compose logs -f app
.PHONY: logs

upgrade: build network
	@ $(APP) yarn upgrade-interactive --latest
.PHONY: upgrade

types: build network
	@ $(APP) yarn generate-types
.PHONY: types

lint: build network
	@ $(APP) yarn lint
.PHONY: lint

test: build network
	@ $(APP) yarn test --passWithNoTests
.PHONY: test

network:
	@ # This network is required if the backend was not spun up yet
	@ docker network create backend_shared_network >/dev/null 2>&1 || true
.PHONY: network

prune:
	@ docker image prune -af
.PHONY: prune

clean:
	@ $(MAKE) prune
.PHONY: clean
