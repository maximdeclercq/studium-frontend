// Copyright: (c) 2020-2021, VTK Gent vzw
// GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/agpl-3.0.txt)

module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'plugin:vue/recommended',
    'plugin:nuxt/recommended',
    'plugin:prettier/recommended',
    'prettier',
  ],
  plugins: ['prettier', 'simple-import-sort'],
  ignorePatterns: ['apollo/_generated.ts'],
  rules: {
    // Import Sorter
    'simple-import-sort/imports': 'error',
    'simple-import-sort/exports': 'error',

    // Composition API Specific Rules
    // These rules are designed for Vue 3, but should also work with Vue 2.
    'vue/no-ref-as-operand': 'error',
    'vue/no-setup-props-destructure': 'error',

    // Error instead of warning on specific rules
    'vue/require-prop-types': 'error',
  },
}
