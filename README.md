# Studium (frontend)

Studium is an online platform for exchanging summaries, tips and learning
experiences.

Studium is free software. See the files whose names start with COPYING for
copying permission.

Copyright years on Studium source files may be listed using range notation,
e.g., 2020-2025, indicating that every year in the range, inclusive, is a
copyrightable year that could otherwise be listed individually.

## Project setup

### Node.JS Version

This project requires [Node.JS 12-15](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/getting-started/install).

### Requirements for Linux or WSL

- Install Git

  ```shell script
  $ sudo apt install git
  ```

- Clone this repository

  ```shell script
  $ git clone https://github.ugent.be/VTK/studium-frontend
  ```

- [Install docker](https://docs.docker.com/engine/install/ubuntu/)

### Requirements for Windows

- Download Git [here](https://git-scm.com/download/win) and install it

- Clone this repository

  ```shell script
  PS> git clone https://github.ugent.be/VTK/studium-frontend
  ```

- [Install Docker for Windows](https://docs.docker.com/engine/install/ubuntu/)

### Setup Webstorm (optional)

- Apply for a free Jetbrains student license [here](https://www.jetbrains.com/shop/eform/students)
- Log in in the Jetbrains Toolbox with your student account
- Install Webstorm from the Jetbrains Toolbox
- Open the cloned repository in Webstorm

### Install browser extensions (optional)

- Vue.js Developer Tools ([Chrome](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd), [Firefox](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/))
- Apollo Client Developer Tools ([Chrome](https://chrome.google.com/webstore/detail/apollo-client-developer-t/jdkknkkbebbapilgoeccciglkfbmbnfmn), ~~Firefox~~)

## Running the server

To speed up the build and leverage caching, BuildKit is used.
If you are not using `make` but `docker-compose` directly, you need to set the following variables to enable BuildKit.

- Unix-like

```shell script
export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1
```

- Powershell

```shell script
$Env:DOCKER_BUILDKIT = 1
$Env:COMPOSE_DOCKER_CLI_BUILD = 1
```

#### Login to the Docker registry

```shell script
docker login registry.gitlab.com
```

#### Build and run the frontend

```shell script
make
```

#### Run tests

```shell script
make lint && make test
```

#### Generate GraphQL queries and types

```shell script
make types
```

#### Upgrade packages

```shell script
make upgrade
```
