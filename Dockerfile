##################
### Base image ###
##################
FROM node:15-alpine as base

# Set working directory
WORKDIR /app

# Set the port that this container exposes
EXPOSE 80/tcp

# Add yarn binaries to path
ENV CACHE_PATH /node_cache
ENV MODULES_PATH /node_modules
ENV PATH $MODULES_PATH/.bin:$PATH

# Add a healthcheck to see if Nuxt.js is healthy
HEALTHCHECK CMD wget --spider --tries 1 http://localhost/

# Install dependencies
RUN apk add python make g++

# Copy configuration
COPY *.json *yarn* ./

# Install required packages
RUN --mount=type=cache,target=/node_cache \
  --mount=type=cache,target=/node_modules/.cache \
  yarn install \
  --production \
  --cache-folder="$CACHE_PATH" \
  --modules-folder="$MODULES_PATH" \
  --frozen-lockfile

#########################
### Development image ###
#########################
FROM base as development

# Enable hot reloading by polling
ENV CHOKIDAR_USEPOLLING=true

# Install development packages
RUN --mount=type=cache,target=/node_cache \
  --mount=type=cache,target=/node_modules/.cache \
  yarn install \
  --cache-folder="$CACHE_PATH" \
  --modules-folder="$MODULES_PATH" \
  --frozen-lockfile

# Copy source
COPY . .

# Start development server with debugger
CMD node --inspect=0.0.0.0:9229 "$MODULES_PATH/.bin/nuxt" --hostname=0.0.0.0 --port=80

#############################
### Bob the Builder image ###
#############################
FROM development as bob

# Build production server
RUN --mount=type=cache,target=/node_modules/.cache \
  yarn build

########################
### Production image ###
########################
FROM base as production

# Copy production server
COPY --from=bob /app/.nuxt .nuxt
COPY --from=bob /app/runtime.config.js nuxt.config.js
COPY --from=bob /app/static static

# Start production server
CMD yarn start --hostname 0.0.0.0 --port 80
