// Copyright: (c) 2020-2021, VTK Gent vzw
// GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/agpl-3.0.txt)

import type {
  HTTPResponse,
  RefreshableScheme,
  RefreshableSchemeOptions,
  SchemeCheck,
  SchemeOptions,
  SchemePartialOptions,
  TokenableSchemeOptions,
} from '@nuxtjs/auth-next'
import type { Auth } from '@nuxtjs/auth-next/dist/runtime'
import { BaseScheme, RefreshController, RefreshToken, Token } from '@nuxtjs/auth-next/dist/runtime'
import type { ApolloClient } from 'apollo-client'
import type { DocumentNode } from 'graphql'

export declare class CasHandler {
  scheme: RefreshableScheme<any>

  constructor(scheme: CasScheme)

  setHeader(token: string): Promise<void>
  clearHeader(): Promise<void>
  initializeApolloLink(refresh: DocumentNode): void
  reset(): void
}

export declare interface CasSchemeOptions extends SchemeOptions, TokenableSchemeOptions, RefreshableSchemeOptions {
  ticket: {
    property: string
  }
  target: {
    property: string
  }
  user: {
    property: string
  }
  redirects: {
    login: string
    logout: string
  }
  callbacks: {
    login: string
    logout: string
  }
  queries: {
    login: DocumentNode
    logout: DocumentNode
    refresh: DocumentNode
    user: DocumentNode
  }
}

export declare class CasScheme<OptionsT extends CasSchemeOptions = CasSchemeOptions>
  extends BaseScheme<OptionsT>
  implements RefreshableScheme
{
  // @ts-ignore
  public token: Token
  // @ts-ignore
  public refreshToken: RefreshToken
  // @ts-ignore
  public refreshController: RefreshController
  // @ts-ignore
  public requestHandler: CasHandler

  public $apolloClient: ApolloClient<any>

  constructor(
    $auth: Auth,
    options: SchemePartialOptions<CasSchemeOptions>,
    ...defaults: SchemePartialOptions<CasSchemeOptions>[]
  )

  mounted(): Promise<HTTPResponse | void>
  reset(): void
  login(): Promise<void>
  logout(): Promise<void>
  setUserToken(token: string | boolean, refreshToken?: string | boolean): Promise<HTTPResponse | void>
  refreshTokens(): Promise<HTTPResponse | void>
  fetchUser(): Promise<void>
  check(checkStatus: boolean): SchemeCheck
}
