// Copyright: (c) 2020-2021, VTK Gent vzw
// GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/agpl-3.0.txt)

import type { NuxtConfig } from '@nuxt/types'

import RuntimeConfig from './runtime.config'
import Seo from './seo.config'

// noinspection JSUnusedGlobalSymbols
const config: NuxtConfig = {
  target: 'server',

  /*
   ** Headers of the page
   */
  head: {
    title: Seo.name,
    titleTemplate: `%s - ${Seo.name}`,
    meta: [
      /**
       * Defaults
       */
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },

      /**
       * SEO
       */

      // General
      {
        hid: 'title',
        name: 'title',
        content: Seo.name,
      },
      {
        hid: 'description',
        name: 'description',
        content: Seo.description,
      },
      {
        hid: 'image',
        name: 'image',
        content: Seo.image,
      },
      {
        hid: 'author',
        name: 'author',
        content: Seo.authors,
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: Seo.keywords,
      },
      {
        hid: 'robots',
        name: 'robots',
        content: 'index, follow',
      },

      // SEO (schema.org for Google)
      {
        hid: 'schema-name',
        itemprop: 'name',
        content: Seo.name,
      },
      {
        hid: 'schema-description',
        itemprop: 'description',
        content: Seo.description,
      },
      {
        hid: 'schema-image',
        itemprop: 'image',
        content: Seo.image,
      },

      // SEO (Twitter)
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary',
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: Seo.name,
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: Seo.description,
      },
      {
        hid: 'twitter:site',
        name: 'twitter:site',
        content: '@oilsjtmjoezik',
      },
      {
        hid: 'twitter:img:src',
        name: 'twitter:img:src',
        content: Seo.image,
      },

      // SEO (Facebook)
      {
        hid: 'og:title',
        property: 'og:title',
        content: Seo.name,
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: Seo.description,
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: Seo.image,
      },
      {
        hid: 'og:site_name',
        property: 'og:site_name',
        content: Seo.domain,
      },
      {
        hid: 'og:locale',
        property: 'og:locale',
        content: 'en_US',
      },
      {
        hid: 'og:type',
        property: 'type',
        content: 'website',
      },

      // Apple
      {
        hid: 'apple-mobile-web-app-title',
        name: 'apple-mobile-web-app-title',
        content: Seo.name,
      },

      // Microsoft
      {
        name: 'msapplication-TileColor',
        content: '#ecf0f1',
      },
      {
        name: 'msapplication-config',
        content: '/browserconfig.xml',
      },
    ],
    link: [
      /**
       * Favicon
       */
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/icons/apple-touch-icon.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/icons/favicon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/icons/favicon-16x16.png',
      },
      {
        rel: 'mask-icon',
        color: '#111b47',
        href: '/icons/safari-pinned-tab.svg',
      },
      {
        rel: 'shortcut icon',
        href: '/icons/favicon.ico',
      },

      /**
       * PWA manifest
       */
      {
        rel: 'manifest',
        href: '/site.webmanifest',
      },
    ],
  },

  /*
   * Global SCSS
   */
  css: [],

  /*
   * Auto import components
   * https://go.nuxtjs.dev/config-components
   */
  components: [
    {
      path: '~/components',
      pathPrefix: false,
      extensions: ['vue'],
    },
  ],

  /*
   * Customize the progress-bar color
   */
  loading: {
    color: '#111b47',
    height: '3px',
  },

  /*
   * Build optimization
   */
  cache: true,
  parallel: true,
  hardSource: true,

  /*
   ** Build configuration
   */
  build: {
    /**
     * Babel configuration
     */
    babel: {
      cacheDirectory: true,
    },

    /**
     * Extend the build configuration
     */
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    },

    /**
     * Transpile dependencies
     */
    transpile: ['gsap'],
  },

  /*
   ** Router configuration
   */
  router: {
    middleware: ['auth'],
  },

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-dompurify-html.ts', mode: 'client' },
    { src: '~/plugins/vue-cookies.ts' },
    { src: '~/plugins/google-gtag.ts' },
    { src: '~/plugins/cookie-consent.ts', mode: 'client' },
  ],

  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxt/typescript-build',
    // Doc: https://composition-api.nuxtjs.org/
    '@nuxtjs/composition-api/module',
    '@nuxtjs/google-fonts',
    // Doc: https://vuetifyjs.com/en/introduction/why-vuetify/
    '@nuxtjs/vuetify',
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // github.com/nuxt-community/apollo-module
    '@nuxtjs/apollo',
    // Doc: https://auth.nuxtjs.org/
    '@nuxtjs/auth-next',
    // Doc: https://github.com/nuxt-community/sentry-module
    '@nuxtjs/sentry',
    // Doc: https://github.com/nuxt-community/proxy-module
    '@nuxtjs/proxy',
  ],

  /*
   ** Apollo module configuration
   ** See https://github.com/nuxt-community/apollo-module#setup
   */
  apollo: {
    clientConfigs: {
      default: '~/apollo/client.ts',
    },
  },

  /*
   ** Auth module configuration
   ** See https://auth.nuxtjs.org/
   */
  auth: {
    cookie: {
      prefix: 'auth.',
      options: {
        path: '/',
        // Expire after 30 days (At least as long as the refresh token expiry)
        maxAge: 30 * 24 * 60 * 60, // in seconds
        // Require a secure connection in production
        secure: process.env.NODE_ENV === 'production',
      },
    },
    strategies: {
      cas: {
        scheme: '~/auth/cas-scheme.ts',
      },
    },
    redirect: {
      home: '/',
      login: '/landing',
      logout: '/landing',
      callback: '/login/callback',
    },
  },

  /*
   ** Google font configuration
   */
  googleFonts: {
    families: {
      Montserrat: [400, 500, 600, 700],
    },
  },

  /*
   ** Sentry module configuration
   ** See https://github.com/nuxt-community/sentry-module
   */
  sentry: {
    dsn: 'https://public@sentry.example.com/1', // dsn is defined in runtime.config.js, but this must be set for Sentry to load
    lazy: true, // Load Sentry lazily so it's not included in your main bundle
  },

  /*
   ** Typescript configuration
   */
  typescript: {
    typeCheck: {
      eslint: {
        files: './**/*.{ts,js,vue}',
      },
    },
  },

  /*
   ** Vuetify configuration
   */
  vuetify: {
    customVariables: ['~/assets/scss/variables.scss'],

    // Theme configuration
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: '#111b47',
          secondary: '#7d8298',
          accent: '#ffb5b5',
          background: '#fbfbfd',
          success: '#4CAF50',
          info: '#2196F3',
          warning: '#FB8C00',
          error: '#FF5252',
        },
        light: {
          primary: '#111b47',
          secondary: '#7d8298',
          accent: '#ffb5b5',
          background: '#fbfbfd',
          success: '#4CAF50',
          info: '#2196F3',
          warning: '#FB8C00',
          error: '#FF5252',
        },
      },
    },

    // Default assets to include
    // This will fetch the assets from an external CDN
    defaultAssets: {
      font: {
        family: 'Montserrat',
      },
      icons: 'mdi',
    },
  },

  /**
   * Runtime variables
   */
  publicRuntimeConfig: RuntimeConfig.publicRuntimeConfig,
  privateRuntimeConfig: RuntimeConfig.privateRuntimeConfig,

  /**
   * Proxy
   */
  proxy: {
    /**
     * Proxy the backend media endpoint via the Nuxt server
     * This is necessary because the backend media endpoint will be blocked for download
     * in modern versions of Chrome and Firefox (https://stackoverflow.com/a/28468261)
     */
    '/media/': RuntimeConfig.privateRuntimeConfig.backendUrl,
  },
}

// noinspection JSUnusedGlobalSymbols
export default config
