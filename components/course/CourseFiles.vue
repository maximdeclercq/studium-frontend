<template>
  <v-container class="studium-tab" transition="page">
    <!-- Files and upload row -->
    <v-row no-gutters class="pt-2 pb-2" align="center" justify="space-between">
      <!-- Title -->
      <v-col cols="6" md="8">
        <span class="studium-title"> Files </span>
      </v-col>

      <!-- Upload button -->
      <v-col cols="auto">
        <v-btn class="studium-button" color="primary" text large :ripple="false" @click="uploadDialog = true">
          Upload
          <v-icon right medium> mdi-upload </v-icon>
        </v-btn>
      </v-col>
    </v-row>

    <!-- Upload success message -->
    <transition name="slide-y-transition" mode="out-in">
      <v-alert v-if="uploadSuccess" class="studium-alert" dense text type="success">
        File has been successfully uploaded
      </v-alert>
    </transition>

    <suspense-query :query="filesQuery">
      <!-- Loading -->
      <template #loading>
        <page-loading class="mt-10" />
      </template>

      <!-- Error -->
      <template #error>
        <page-error :error="filesError" />
      </template>

      <!-- Data -->
      <template #data>
        <!-- Search Bar -->
        <template v-if="filesAvailable">
          <v-text-field
            v-model="search"
            class="studium-input"
            label="Search for a file"
            color="primary"
            hide-details="auto"
            outlined
            clearable
          >
            <template #append>
              <v-icon>mdi-magnify</v-icon>
              <v-icon
                v-if="$vuetify.breakpoint.smAndDown"
                :color="filterVisible ? 'primary' : ''"
                @click="filterVisible = !filterVisible"
              >
                mdi-dots-vertical
              </v-icon>
            </template>
          </v-text-field>
        </template>

        <!-- Filters & Sorting -->
        <transition name="slide-y-transition" mode="out-in">
          <div v-if="filesAvailable && filterVisible">
            <!-- Filters -->
            <v-chip-group v-model="tagsSelected" class="mt-2" multiple>
              <v-chip
                v-for="tag in tags"
                :key="tag.id"
                :value="tag.name"
                class="studium-chip"
                active-class="studium-chip--active"
                outlined
                filter
                flat
                :ripple="false"
              >
                {{ tag.name }}
              </v-chip>
            </v-chip-group>

            <!-- Sorting -->
            <v-slide-group class="mt-4">
              <!-- Upload date -->
              <v-slide-item>
                <course-files-sort-btn :sort-selected.sync="sortSelected" property="uploadDate" text="Date" />
              </v-slide-item>

              <!-- Title -->
              <v-slide-item>
                <course-files-sort-btn :sort-selected.sync="sortSelected" property="name" text="Name" />
              </v-slide-item>

              <!-- Downloads -->
              <v-slide-item>
                <course-files-sort-btn :sort-selected.sync="sortSelected" property="downloadCount" text="Downloads" />
              </v-slide-item>

              <!-- Average Ratings -->
              <v-slide-item>
                <course-files-sort-btn :sort-selected.sync="sortSelected" property="averageRatings" text="Ratings" />
              </v-slide-item>
            </v-slide-group>
          </div>
        </transition>

        <!-- Files -->
        <transition name="fade" mode="out-in">
          <!-- No files available -->
          <template v-if="!filesAvailable">
            <placeholder brackets class="mt-6" @click="uploadDialog = true">
              <template #first> No files available for this course. </template>
              <template #second> Upload a file! </template>
            </placeholder>
          </template>

          <!-- No files available for this query -->
          <template v-else-if="!filesFilteredAvailable">
            <placeholder class="mt-6">
              <template #first> No results found... Try searching something else. </template>
            </placeholder>
          </template>

          <!-- Files -->
          <course-file-list
            v-else
            :files="filesFiltered"
            :page.sync="filesPage"
            class="mt-4"
            :course-code="courseCode"
          />
        </transition>

        <!-- Upload Dialog -->
        <!-- SSR does not see hash so client and server -->
        <!-- have differen values => error, hence only -->
        <!-- client side rendering -->
        <client-only>
          <course-file-upload-dialog
            v-if="uploadDialog"
            :open.sync="uploadDialog"
            :course-code="courseCode"
            :tags="tags"
            @success="onUploadSuccess"
          />
        </client-only>
      </template>
    </suspense-query>
  </v-container>
</template>

<script lang="ts">
import { computed, defineComponent, reactive, ref, watch } from '@nuxtjs/composition-api'
import { useResult } from '@vue/apollo-composable'
import { useDebounce } from '@vueuse/shared'
import { parseISO } from 'date-fns'

import { DocumentInfoFragment, useCourseDocumentsQuery, useDocumentTagsQuery } from '~/apollo/_generated'
import { useErrorHandler, useRouterHash } from '~/util/composables'
import { isDate, isNumeric } from '~/util/helpers'

export default defineComponent({
  props: {
    // Code of the course
    courseCode: {
      type: String,
      default: '',
    },
  },
  setup(props, { root }) {
    // Search bar value
    const search = ref('')
    const searchDebounced = useDebounce(search, 200)

    // Tags query & items
    const tagsQuery = useDocumentTagsQuery()
    const tags = useResult(tagsQuery.result, [], (result) =>
      result?.allDocumentTags?.edges.flatMap((edge) => edge?.node)
    )
    const tagsSelected = ref<string[]>([])

    // Sort options
    const sortSelected = reactive({
      property: 'uploadDate',
      order: 'desc',
    })

    // Course files query & items
    const filesQuery = useCourseDocumentsQuery({ code: props.courseCode })
    const filesError = useErrorHandler(filesQuery)
    const files = useResult(
      filesQuery.result,
      [],
      (result) => result?.courseByCode?.documents.edges.flatMap((edge) => edge?.node) as DocumentInfoFragment[]
    )

    // Files pagination index
    const filesPage = ref(1)

    // Reset the page on search
    watch(
      () => [search.value, tagsSelected.value],
      () => {
        filesPage.value = 1
      }
    )

    // If there is at least one file
    const filesAvailable = computed(() => (files.value ? files.value?.length > 0 : false))

    // Filtered files
    // Will use the search bar & selected tags to determin which files to display
    // Will sort the resulting data based on the selected sort order
    const filesFiltered = computed(() =>
      files.value
        // Filter files that match the search query.
        .filter((file) =>
          file.name.toLowerCase().includes(searchDebounced.value ? searchDebounced.value.toLowerCase() : '')
        )

        // Filter files that match the selected tags
        .filter(
          (file) =>
            tagsSelected.value.length === 0 ||
            file?.tags.edges.find((tag) => (tag?.node?.name ? tagsSelected.value?.includes(tag?.node?.name) : false))
        )

        // Sort the data based on the selected sort property & order
        .sort((a, b) => {
          const aProperty: any = a[sortSelected.property]
          const bProperty: any = b[sortSelected.property]

          // Sorting compare value
          let compare = 0

          // Compare functions for sorting.
          // Compare dates with by their time value
          if (isDate(aProperty) && isDate(bProperty)) {
            compare = parseISO(aProperty).getTime() - parseISO(bProperty).getTime()
          }

          // Compare numbers by their value
          else if ((isNumeric(aProperty) || aProperty === null) && (isNumeric(bProperty) || bProperty == null)) {
            compare = Number(aProperty) - Number(bProperty)
          }

          // Compare strings with localCompare
          else {
            // Convert all other types to primitive strings and compare using localCompare
            compare = String(aProperty).localeCompare(String(bProperty))
          }

          // Sort ascending or descending
          if (sortSelected.order === 'desc') {
            compare *= -1
          }

          return compare
        })
    )

    // If there is at least one filtered file
    const filesFilteredAvailable = computed(() => (filesFiltered.value ? filesFiltered.value?.length > 0 : false))

    // Should the filters be visible
    // Filters will be visible by default on desktop and hidden on mobile.
    const filterVisible = ref(true)

    // Hide filters when going mobile or when no search results are available
    watch(
      () => root.$vuetify.breakpoint.smAndDown,
      (isMobile) => {
        // Hide the filter when switching to mobile.
        if (isMobile) {
          filterVisible.value = false
        } else {
          filterVisible.value = true
        }
      },
      { immediate: true }
    )

    // Should the upload dialog be open
    const uploadDialog = useRouterHash('upload')

    // Should the upload success message be displayed
    const uploadSuccess = ref(false)

    // Upload success event
    function onUploadSuccess() {
      uploadSuccess.value = true

      // Clear the success message after 5 seconds
      setTimeout(() => {
        uploadSuccess.value = false
      }, 5000)
    }

    return {
      search,
      tagsQuery,
      tags,
      tagsSelected,
      sortSelected,
      filesQuery,
      filesError,
      files,
      filesAvailable,
      filesPage,
      filesFiltered,
      filesFilteredAvailable,
      filterVisible,
      uploadDialog,
      uploadSuccess,
      onUploadSuccess,
    }
  },
})
</script>
