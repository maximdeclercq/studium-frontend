const { startDevServer } = require('@cypress/webpack-dev-server')
const { getWebpackConfig } = require('nuxt')

module.exports = (on, config) => {
  // Start development server
  on('dev-server:start', async (options) => {
    const webpackConfig = await getWebpackConfig()
    return startDevServer({ options, webpackConfig })
  })

  // // Generate coverage report
  // require('@cypress/code-coverage/task')(on, config)
  // on('file:preprocessor', require('@cypress/code-coverage/use-browserify-istanbul'))

  return config
}
