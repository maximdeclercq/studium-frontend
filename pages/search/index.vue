<!--
  Copyright: (c) 2020-2021, VTK Gent vzw
  GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/agpl-3.0.txt)
-->

<template>
  <div>
    <!-- Search -->
    <v-row align="center">
      <!-- Search Bar -->
      <v-col cols="auto" class="flex-grow-1">
        <v-text-field
          v-model="search"
          :loading="coursesQuery.loading.value"
          :error-messages="coursesQueryError ? coursesQueryError.message : null"
          class="studium-input"
          label="Search for a course"
          color="primary"
          hide-details="auto"
          outlined
          clearable
          @keyup.enter="onSearchEnter"
        >
          <template slot="append">
            <v-icon v-if="search === ''">mdi-magnify</v-icon>

            <!-- Expand filters on mobile icon -->
            <v-icon
              v-if="$vuetify.breakpoint.xsOnly"
              :color="filterVisible ? 'primary' : ''"
              @click="filterVisible = !filterVisible"
            >
              mdi-dots-vertical
            </v-icon>
          </template>
        </v-text-field>
      </v-col>
    </v-row>

    <!-- Filters -->
    <transition name="slide-y-transition" mode="out-in">
      <v-row v-if="filterVisible" justify="space-between">
        <!-- Faculty -->
        <v-col cols="12" sm="6" md="3" lg="3">
          <v-tooltip bottom :disabled="!filter.program">
            <template #activator="{ on, attrs }">
              <div v-bind="attrs" v-on="on">
                <v-autocomplete
                  v-model="filter.faculty"
                  :error-messages="facultiesQueryError ? facultiesQueryError.message : null"
                  :items="faculties"
                  :disabled="!!filter.program"
                  class="studium-input"
                  label="Faculty"
                  item-text="name"
                  item-value="code"
                  hide-details="auto"
                  outlined
                  clearable
                  single-line
                  dense
                  autocomplete="off"
                />
              </div>
            </template>

            <!-- Tell the user they need to select a file to continue -->
            <span> You don't need to select a faculty when a program is already selected </span>
          </v-tooltip>
        </v-col>

        <!-- Program -->
        <v-col cols="12" sm="6" md="3" lg="3">
          <v-autocomplete
            v-model="filter.program"
            :error-messages="programsQueryError ? programsQueryError.message : null"
            :items="programs"
            class="studium-input"
            label="Program"
            item-text="name"
            item-value="code"
            hide-details="auto"
            outlined
            clearable
            single-line
            dense
            autocomplete="off"
          />
        </v-col>

        <!-- Year -->
        <v-col cols="12" sm="6" md="3" lg="3">
          <v-tooltip bottom :disabled="!!filter.program">
            <template #activator="{ on, attrs }">
              <div v-bind="attrs" v-on="on">
                <v-autocomplete
                  v-model="filter.year"
                  :error-messages="yearsQueryError ? yearsQueryError.message : null"
                  :items="years"
                  :disabled="!filter.program"
                  class="studium-input"
                  label="Years"
                  hide-details="auto"
                  outlined
                  clearable
                  single-line
                  dense
                  autocomplete="off"
                />
              </div>
            </template>

            <!-- Tell the user they need to select a file to continue -->
            <span> Select a program before selecting a year </span>
          </v-tooltip>
        </v-col>

        <!-- Semester -->
        <v-col cols="12" sm="6" md="3" lg="3">
          <v-tooltip bottom :disabled="!!filter.program">
            <template #activator="{ on, attrs }">
              <div v-bind="attrs" v-on="on">
                <v-autocomplete
                  v-model="filter.semester"
                  :error-messages="semestersQueryError ? semestersQueryError.message : null"
                  :items="semesters"
                  :disabled="!filter.program"
                  class="studium-input"
                  label="Semesters"
                  item-text="name"
                  item-value="value"
                  hide-details="auto"
                  outlined
                  clearable
                  single-line
                  dense
                  autocomplete="off"
                />
              </div>
            </template>

            <!-- Tell the user they need to select a file to continue -->
            <span> Select a program before selecting a semester </span>
          </v-tooltip>
        </v-col>
      </v-row>
    </transition>

    <transition name="fade" mode="out-in">
      <!-- Initial message -->
      <placeholder v-if="!hasSearched" key="initial" class="pt-6">
        <template #first> Search for a course or add some filters...</template>
      </placeholder>

      <!-- No courses available -->
      <placeholder v-else-if="!courses || courses.length === 0" key="empty" class="pt-6">
        <template #first> No courses found with the given search terms</template>
      </placeholder>

      <!-- Courses -->
      <course-card-list v-else key="courses" class="pt-6" :courses="courses" :page.sync="coursesPage" />
    </transition>
  </div>
</template>

<script lang="ts">
import { defineComponent, reactive, ref, useMeta, watch } from '@nuxtjs/composition-api'
import { useResult } from '@vue/apollo-composable'
import { NetworkStatus } from 'apollo-client'

import {
  useAllSemestersQuery,
  useAllYearsQuery,
  useSearchCoursesQuery,
  useSearchFacultyQuery,
  useSearchProgramQuery,
} from '~/apollo/operations'
import { useErrorHandler, useRouterQuery } from '~/util/composables'

export default defineComponent({
  setup(_, { root }) {
    const isEmpty = (v) => v === null || v === ''

    // Setup searchbar value (default from route)
    const search = useRouterQuery('search', '', { isEmpty })

    // Setup filter values (default from route)
    const filter = reactive<any>({
      faculty: useRouterQuery('faculty', '', { isEmpty }),
      program: useRouterQuery('program', '', { isEmpty }),
      year: useRouterQuery('year', '', { isEmpty }),
      semester: useRouterQuery('semester', '', { isEmpty }),
    })

    // Initialize courses query
    const coursesQuery = useSearchCoursesQuery(
      { search: search.value, ...filter },
      { throttle: 500, notifyOnNetworkStatusChange: true }
    )
    const coursesQueryError = useErrorHandler(coursesQuery)
    const courses = useResult(coursesQuery.result, [], (result) =>
      result?.allCourses?.edges.flatMap((edge) => edge?.node)
    )

    // Current page of results
    const coursesPage = ref(1)

    // Listen to the courseQuery and reset the search page to 1 on refetch
    coursesQuery.onResult(() => {
      if (coursesQuery.networkStatus.value === NetworkStatus.refetch) {
        coursesPage.value = 1
      }
    })

    // Setup filter search values
    const filterSearch = reactive<any>({ faculty: null, program: null, year: null, semester: null, course: null })

    // Should the filters be visible
    // Filters will be visible by default on desktop and hidden on mobile.
    const filterVisible = ref(false)

    // Hide filters when going mobile
    watch(
      () => root.$vuetify.breakpoint.xsOnly,
      (isMobile) => {
        // Hide the filter when switching to mobile.
        filterVisible.value = !isMobile
      },
      { immediate: true }
    )

    // Initialize faculties query
    const facultiesQuery = useSearchFacultyQuery()
    const facultiesQueryError = useErrorHandler(facultiesQuery)
    const faculties = useResult(facultiesQuery.result, [], (result) =>
      result?.allFaculties?.edges.flatMap((edge) => edge?.node)
    )

    // Initialize programs query
    const programsQuery = useSearchProgramQuery({ faculty: filter.faculty })
    const programsQueryError = useErrorHandler(programsQuery)
    const programs = useResult(programsQuery.result, [], (result) =>
      result?.allPrograms?.edges.flatMap((edge) => edge?.node)
    )

    // Initialize years query
    const yearsQuery = useAllYearsQuery({ program: filter.program })
    const yearsQueryError = useErrorHandler(yearsQuery)
    const years = useResult(yearsQuery.result, [])

    // Initialize semesters query
    const semestersQuery = useAllSemestersQuery()
    const semestersQueryError = useErrorHandler(semestersQuery)
    const semesters = useResult(semestersQuery.result, [], (result) =>
      result?.allSemesters?.map((semester) => ({
        name: semester || 'Not Specified',
        value: semester,
      }))
    )

    // If the user has searched or filtered any data
    const hasSearched = ref(false)

    // When the user presses enter on the searchfield
    // refetch the courses
    function onSearchEnter() {
      // Not the debounced value for search, since the user
      // explicitly wants to query the current search value.
      coursesQuery.refetch({
        search: search.value,
        ...filter,
      })
    }

    // Update "hasSearched" when the search query is done loading.
    watch(
      () => coursesQuery.loading.value,
      (loading) => {
        if (!loading) {
          hasSearched.value =
            (search.value ? search.value.length !== 0 : false) ||
            filter.faculty !== null ||
            filter.program !== null ||
            filter.year !== null ||
            filter.semester !== null
        }
      },
      { immediate: true }
    )

    // Update the courses when:
    // - the user types something in the search bar
    // - the filters get updated
    watch(
      () => [search.value, filter.faculty, filter.program, filter.year, filter.semester],
      ([search, faculty, program, year, semester]) => {
        // Do not refetch when still loading
        if (!coursesQuery.loading?.value) {
          coursesQuery.refetch({ faculty, program, year, semester, search })
        }
      }
    )

    // Update the programs when:
    // - the user selects a faculty
    watch(
      () => filter.faculty,
      (faculty) => {
        // Do not refetch when still loading
        if (!programsQuery.loading?.value) {
          programsQuery.refetch({ faculty })
        }
      }
    )

    // Update the years when:
    // - the user selects a program
    watch(
      () => filter.program,
      (program) => {
        // Do not refetch when still loading
        if (!yearsQuery.loading?.value) {
          yearsQuery.refetch({ program })
          filter.year = null
        }
      }
    )

    // SEO
    useMeta(() => ({
      title: 'Search',
    }))

    return {
      search,
      coursesQuery,
      coursesQueryError,
      courses,
      coursesPage,
      filter,
      filterSearch,
      filterVisible,
      facultiesQuery,
      facultiesQueryError,
      faculties,
      programsQuery,
      programsQueryError,
      programs,
      yearsQuery,
      yearsQueryError,
      years,
      semestersQuery,
      semestersQueryError,
      semesters,
      hasSearched,
      onSearchEnter,
    }
  },

  // Empty head for useMeta
  head: {},
})
</script>
