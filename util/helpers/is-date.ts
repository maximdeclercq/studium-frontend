import { isValid, parseISO } from 'date-fns'

/**
 * If a given string is a date in a given format.
 * @param {string} str the string to check.
 */
export function isDate(str: string) {
  const parsedDate = parseISO(str)

  return isValid(parsedDate)
}
