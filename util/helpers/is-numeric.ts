/**
 * If a given string is numeric.
 * @param {string} str the string to check.
 */
export function isNumeric(str: string) {
  return !isNaN(parseFloat(str))
}
