// Copyright: (c) 2020-2021, VTK Gent vzw
// GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/agpl-3.0.txt)
import * as fs from 'fs'

// Get a variable from a file, or if it doesn't exists, from an env var
function readFileOrGetEnv(fileName, envName) {
  if (fs.existsSync(fileName)) {
    return fs.readFileSync(fileName, { encoding: 'utf8', flag: 'r' })
  }
  return process.env[envName]
}

// Export RuntimeConfig
export default {
  /**
   * Runtime configurations
   */
  publicRuntimeConfig: {
    baseUrl: process.env.NUXT_BASE_URL || 'http://localhost:4000',
    backendUrl: process.env.NUXT_BACKEND_PUBLIC_URL || 'http://localhost:5000',
    mockCas: process.env.NUXT_MOCK_CAS || process.env.NODE_ENV !== 'production',
    gtagId: readFileOrGetEnv('/run/secrets/nuxt_google_gtag_id', 'NUXT_GOOGLE_GTAG_ID'),
    sentry: {
      config: {
        dsn: readFileOrGetEnv('/run/secrets/nuxt_sentry_dsn', 'NUXT_SENTRY_DSN'),
      },
    },
  },
  privateRuntimeConfig: {
    backendUrl: process.env.NUXT_BACKEND_PRIVATE_URL || 'http://backend',
  },
}
