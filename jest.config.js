// Copyright: (c) 2020-2021, VTK Gent vzw
// GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/agpl-3.0.txt)

module.exports = {
  // Junit reporter configuration
  reporters: ['default', ['jest-junit', { outputDirectory: '<rootDir>/out', outputName: 'junit.xml' }]],

  // Cobertura reporter configuration
  coverageReporters: [
    ['cobertura', { file: 'out/cobertura.xml' }],
    ['text', { skipEmpty: true, skipFull: false }],
  ],

  // Coverage configuration
  collectCoverage: true,
  collectCoverageFrom: ['<rootDir>/components/**/*.vue', '<rootDir>/pages/**/*.vue'],
  coverageDirectory: '<rootDir>/out',

  // Ignore Cypress tests
  testPathIgnorePatterns: ['/cypress/', '/node_modules/'],

  // Modules configuration
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js',
  },
  moduleFileExtensions: ['ts', 'js', 'vue', 'json'],

  // Transformer configuration
  transform: {
    '^.+\\.ts$': 'ts-jest',
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest',
  },

  // Enable verbose output
  verbose: true,
}
