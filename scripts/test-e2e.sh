#!/bin/sh
set -ex

# Speedup docker build
export COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 DOCKER_DRIVER=overlay2

# Shortcut
export N="test_e2e"

# Build production frontend
TARGET=production docker build --tag=${N}_frontend .

# Create network for testing
docker network create "${N}"
alias cleanup_network="docker network rm "${N}" >/dev/null"
trap cleanup_network EXIT

# Setup backend
docker container run --rm --network="${N}" --name="${N}_backend" -d "registry.gitlab.com/vtkgent/studium/backend:development"
alias cleanup_backend="docker container stop ${N}_backend >/dev/null"
trap cleanup_backend EXIT

# Setup frontend
docker container run --rm --network="${N}" --name="${N}_frontend" -d -p="6000:80" "${N}_frontend"
alias cleanup_frontend="docker container stop ${N}_frontend >/dev/null"
trap cleanup_frontend EXIT

# Perform tests
yarn test:e2e
